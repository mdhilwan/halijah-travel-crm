# README #

Halijah Travel CRM

### What is this repository for? ###

* Customer Details listing
* Name Tag Generator
* Luggage Tag Generator
* VISA Form Generator

### Stack

* React + Redux => [create-react-app](https://github.com/facebook/create-react-app)
* Java Spring Boot
* Scanner reader => [scanner.js](https://www.npmjs.com/package/scanner-js) 
* Optical char recogntion => [ocrad.js](http://antimatter15.com/ocrad.js/demo.html)

### Starting Frontend

* cd crm-frontend/ && yarn start

### TODO:

* Backend start up