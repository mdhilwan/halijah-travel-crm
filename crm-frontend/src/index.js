import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
import 'datatables.net-bs4/css/dataTables.bootstrap4.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
