import React, { Component } from 'react';
import { Alert, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class OpenScannerModalButton extends Component {

	constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.openScannerModal = this.openScannerModal.bind(this);
  }

	openScannerModal () {
    this.setState({
      modal: !this.state.modal
    });
  }

	render() {
		return (
			<div> 
				<a className="dropdown-item btn" onClick={this.openScannerModal}>
	      	<i className="fas fa-print"></i> Import From Scanner
      	</a>
      	<Modal isOpen={this.state.modal} toggle={this.openScannerModal.bind(this)} className="openScannerModal" size="lg">
          <ModalHeader toggle={this.openScannerModal}>Scan Document</ModalHeader>
          <ModalBody>
          	<Alert color="warning">
        			<h5>Positon the document properly</h5>
        			Ensure that the document is placed properly on the scanner bed. It's is best to ensure that the document is as straight as possible.
      			</Alert>
						<div className="scanning">
							Hello World
						</div>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.startScanning}>Start Scanning</Button>{' '}
            <Button color="secondary" onClick={this.openScannerModal}>Cancel</Button>
          </ModalFooter>
        </Modal>
			</div>
		);
	}
}

export default OpenScannerModalButton;