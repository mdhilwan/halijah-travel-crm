import React, { Component } from 'react';

import MainTopNav from './MainTopNav';
import MainSideNav from './MainSideNav';

class MainNav extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		    <a className="navbar-brand" href="index.html">Halijah Travel - CRM</a>
		    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
		      <span className="navbar-toggler-icon"></span>
		    </button>
		    <div className="collapse navbar-collapse" id="navbarResponsive">
		      <MainSideNav />
		      <MainTopNav />
  			</div>
  		</nav>
		)
	}
}

export default MainNav;