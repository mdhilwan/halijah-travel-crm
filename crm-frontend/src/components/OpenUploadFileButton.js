import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class OpenUploadFileButton extends Component {

	constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.uploadFileModal = this.uploadFileModal.bind(this);
  }

	uploadFileModal() {
    this.setState({
      modal: !this.state.modal
    });
  }

	render() {
		return (
			<div> 
				<a className="dropdown-item btn" onClick={this.uploadFileModal}>
	      	<i className="fas fa-upload"></i> Upload File
      	</a>
      	<Modal isOpen={this.state.modal} toggle={this.uploadFileModal.bind(this)} className="uploadFileModal">
          <ModalHeader toggle={this.uploadFileModal}>Upload Document</ModalHeader>
          <ModalBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.uploadFileModal}>Do Something</Button>{' '}
            <Button color="secondary" onClick={this.uploadFileModal}>Cancel</Button>
          </ModalFooter>
        </Modal>
			</div>
		);
	}
}

export default OpenUploadFileButton;