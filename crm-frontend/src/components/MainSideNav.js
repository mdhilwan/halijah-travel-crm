import React, { Component } from 'react';

class MainSideNav extends Component {
	render() {
		return (
			<div>
	      <ul className="navbar-nav navbar-sidenav" id="exampleAccordion">
	        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
	          <a className="nav-link" href="index.html">
	            <i className="fa fa-fw fa-users"></i>
	            <span className="nav-link-text">Customer Details</span>
	          </a>
	        </li>
	        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
	          <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
	            <i className="fa fa-fw fa-wrench"></i>
	            <span className="nav-link-text">Settings</span>
	            <span	className="nav-arrow">
	            	<i className="fas fa-angle-right"></i>
	            </span>
	          </a>
	          <ul className="sidenav-second-level collapse" id="collapseComponents">
	            <li>
	              <a href="navbar.html">Reset Password</a>
	            </li>
	          </ul>
	        </li>
	      </ul>
	      <ul className="navbar-nav sidenav-toggler">
	        <li className="nav-item">
	          <a className="nav-link text-center" id="sidenavToggler">
	            <i className="fa fa-fw fa-angle-left"></i>
	          </a>
	        </li>
	      </ul>
      </div>
		);
	}
}

export default MainSideNav;