import React, { Component } from 'react';
import * as $ from 'jquery';
import moment from 'moment';

require( 'datatables.net-bs4' );

class CustomerList extends Component {
  componentDidMount () {
    $('#dataTable').DataTable({
      ajax: {
        "url": "http://localhost:4000/customers",
        "dataSrc": ""
      },
      columns: [
        { data: "registered" },
        { data: "name" },
        { data: "nric" },
        { data: "passportNumber" },
        { data: "dob" },
        { data: "dob" },
        { data: "gender" }
      ],
      createdRow: function (row, data, dataIndex) {
        let formatDateCell = (eq, type) => {
          let cell = $(row).find('td:eq(' + eq + ')');
          let dateOrder = cell.text();
          if (type == 'date') cell.data('order', dateOrder).text(moment(dateOrder).format('d MMM YYYY'));
          else if (type == 'age') cell.data('order', dateOrder).text(moment().diff(moment(dateOrder), 'years'))
        }
        formatDateCell(0, 'date'); 
        formatDateCell(4, 'date'); 
        formatDateCell(5, 'age'); 
      }
    });
  }

  render() {
    return (
      <div className="card mb-3">
        <div className="card-header">
          <i className="fa fa-table"></i> Customer Details
        </div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
              <thead>
                <tr>
                  <th>Date Registered</th>
                  <th>Name</th>
                  <th>NRIC</th>
                  <th>Passport Number</th>
                  <th>Date of Birth</th>
                  <th>Age</th>
                  <th>Gender</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Date Registered</th>
                  <th>Name</th>
                  <th>NRIC</th>
                  <th>Passport Number</th>
                  <th>Date of Birth</th>
                  <th>Age</th>
                  <th>Gender</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    );
  }
}

export default CustomerList;
