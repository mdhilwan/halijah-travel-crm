import React, { Component } from 'react';
import OpenScannerModalButton from './OpenScannerModalButton';
import OpenUploadFileButton from './OpenUploadFileButton';

class CustomerListToolbar extends Component {

	render() {
		return (
			<div className="btn-group mb-3" role="group" aria-label="Basic example">
			  <div className="btn-group" role="group">
			  	<button id="btnGroupDrop1" type="button" className="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			  		<i className="fas fa-plus"></i> New Customer
			  	</button>
			    <div className="dropdown-menu" aria-labelledby="btnGroupDrop1">
			    	<OpenScannerModalButton />
			    	<OpenUploadFileButton />
			    </div>
			  </div>
			  <button type="button" className="btn btn-light">Generate Name Tag</button>
			  <button type="button" className="btn btn-light">Generate Baggage Tag</button>
			  <button type="button" className="btn btn-light">Generate Visa Form</button>
			</div>
		);
	}
}

export default CustomerListToolbar;