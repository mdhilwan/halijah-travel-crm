import React, { Component } from 'react';
import ReactDOM from 'react-dom'

//import './App.css';

import '@fortawesome/fontawesome';
import '@fortawesome/fontawesome-free-solid';
import '@fortawesome/fontawesome-free-webfonts';

import './includes/jquery';
import './includes/bootstrap';

import MainNav from './components/MainNav';
import CustomerList from './components/CustomerList';
import CustomerListToolbar from './components/CustomerListToolbar';

class App extends Component {
  render() {
    return (
      <div className="App">
      	<MainNav />
        <div className="content-wrapper">
          <div className="container-fluid">
            <CustomerListToolbar />
          	<CustomerList />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
