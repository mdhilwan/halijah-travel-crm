This is test assignment for octus.
This demo is for handling user accounts at the system using restful API and JSON web token authentication. 

Used cloud hosted database from mlab. 
JWT token is configured for the api request. 

Token can be generated in 2 ways. 
1. request token with firstName
2. register new customer and get the token

Remark
Other 4 request need token to be present at the header for token authentication. 

Restful methods.

-get token by firstName
API URL - http://127.0.0.1:8080/getToken/firstName
Body JSON String - 
{
    "firstName":"htoo2"
}

-register and get token
API URL - http://127.0.0.1:8080/getToken/registerCustomer
Body -  
{
    "firstName":"htoo",
    "lastName":"franzy"
}

-Find all customers
API URL - http://127.0.0.1:8080/customer/all
Header - Authorization , Bearer returnedTokenFromBackend

-find one customer
API URL - http://127.0.0.1:8080/customer/find
Body - JSON String
{
	"customerId":"58a264ac61ab1c9868743f65"
}
Header - Authorization , Bearer returnedTokenFromBackend

-delete one customer
API URL - http://127.0.0.1:8080/customer/delete
Body - JSON String 
{
	"customerId":"58a264ac61ab1c9868743f66"
}
Header - Authorization , Bearer returnedTokenFromBackend

-modify one customer
API URL - http://127.0.0.1:8080/customer/modify
Body - JSON String
{
	"customerId":"58a264ac61ab1c9868743f65",
	"firstName":"John2",
	"lastName":"Smith2"
}
Header - Authorization , Bearer returnedTokenFromBackend

The postman collection test cases are attached as Octus.postman_collection.json 
