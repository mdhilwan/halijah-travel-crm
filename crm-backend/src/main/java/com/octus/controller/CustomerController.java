package com.octus.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.octus.model.Customer;
import com.octus.service.CustomerService;



@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	

	
	@RequestMapping("/all")
	public List<Customer> findAllCustomers() {
		System.out.println("requested to show all customers");
		return customerService.findAllCustomer();
	}
	
	@RequestMapping(value="/find")
	public Customer findCustomer(@RequestBody Map<String, String> json) {
		
		String customerId = json.get("customerId");		
		System.out.println("find customer:"+customerId);		
		return customerService.find(customerId);
	}
	@RequestMapping(value="/firstName")
	public Customer findCustomerByFirstName(@RequestBody Map<String, String> json) {

		String customerId = json.get("firstName");
		System.out.println("find customer:"+customerId);
		return customerService.findByFirstName(customerId);
	}
	@RequestMapping(value="/delete")
	public String deleteCustomer(@RequestBody Map<String, String> json) {
		
		String customerId = json.get("customerId");		
		System.out.println("delete customer:"+customerId);		
		customerService.delete(customerId);		
		return "deleted";
	}
	
	@RequestMapping(value="/modify")
	public Customer modifyFirstname(@RequestBody Map<String, String> json) {
		
		String customerId = json.get("customerId");
		String firstName = json.get("firstName");
		String lastName = json.get("lastName");
		System.out.println("modify customer:"+customerId);		
		return customerService.modifyCustomerName(customerId, firstName, lastName);
	}

	
}


