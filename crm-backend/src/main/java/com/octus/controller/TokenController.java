package com.octus.controller;


import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.octus.model.Customer;
import com.octus.service.CustomerService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/getToken")
public class TokenController {

	@Autowired
	private CustomerService customerService;
	
	@RequestMapping(value="firstName", method=RequestMethod.POST)
	public String getToken(@RequestBody Map<String, String> json) throws ServletException {
		String firstName = json.get("firstName");
		if( firstName== null) {
			throw new ServletException("Please fill in ID");
		}
		
		Customer customer = customerService.findByFirstName(firstName);
		
		
		if(customer == null) {
			throw new ServletException ("User not found.");
		}			
		
		return Jwts.builder().setSubject(firstName).claim("roles", "user").setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, "secretkey").compact();
		
	}	
	
	@RequestMapping(value="/registerCustomer", method = RequestMethod.POST)
	public String registerUser(@RequestBody Customer customer) {
		
		System.out.println("Customer info received for registration");		
		Customer savedCustomer;
		savedCustomer = customerService.save(customer);
		String firstName = savedCustomer.getfirstName();
		return Jwts.builder().setSubject(firstName).claim("roles", "user").setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, "secretkey").compact();
	}

}
