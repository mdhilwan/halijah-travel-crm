package com.octus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;


import com.octus.config.JwtFilter;
@SpringBootApplication
public class OctusTasklistApplication {


	
	public static void main(String[] args) {
		SpringApplication.run(OctusTasklistApplication.class, args);
	}
	
}
