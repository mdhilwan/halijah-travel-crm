package com.octus.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.octus.model.Customer;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface CustomerRepository extends MongoRepository<Customer, String> , QueryDslPredicateExecutor<Customer>{

    @Query("{ firstName: ?0 }")
    Customer findByFirstName(String firstName);

    @Query("{ id: ?0 }")
    Customer findById(String id);


    
}