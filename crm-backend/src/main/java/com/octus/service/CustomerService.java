package com.octus.service;

import java.util.List;

import com.octus.model.Customer;


public interface CustomerService {
	
	List<Customer> findAllCustomer();
	
	Customer save(Customer customer);
	
	Customer find(String id);
	
	void delete(String id);

	Customer modifyCustomerName(String id, String firstname, String lastname);
	
	Customer findByFirstName(String firstName);
}
