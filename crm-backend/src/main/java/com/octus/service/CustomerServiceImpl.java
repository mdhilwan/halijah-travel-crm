package com.octus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.octus.dao.CustomerRepository;
import com.octus.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService{

	@Autowired
    private CustomerRepository customerRepository;

	@Autowired
    MongoTemplate mongoTemplate;

	@Override
	public List<Customer> findAllCustomer() {
		// TODO Auto-generated method stub
		//return customerRepository.findAllCustomer(new Sort(Sort.DEFAULT_DIRECTION));
		return customerRepository.findAll();
	}

	@Override
	public Customer save(Customer customer) {
		// TODO Auto-generated method stub
		return customerRepository.save(customer);
	}

	@Override
	public Customer find(String id) {
		// TODO Auto-generated method stub
		return customerRepository.findById(id);
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		customerRepository.delete(id);		
		
	}

	@Override
	public Customer modifyCustomerName(String id, String firstname, String lastname) {
		// TODO Auto-generated method stub
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));

		Update update = new Update();
		update.set("firstName", firstname);
		update.set("lastName", lastname);

		//FindAndModifyOptions().returnNew(true) = newly updated document
		//FindAndModifyOptions().returnNew(false) = old document (not update yet)
		Customer customer = mongoTemplate.findAndModify(query, update,new FindAndModifyOptions().returnNew(true), Customer.class);


		return customer;
	}

	@Override
	public Customer findByFirstName(String firstName) {
		// TODO Auto-generated method stub
		return customerRepository.findByFirstName(firstName);
	}



}
